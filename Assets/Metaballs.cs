﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class Metaballs: MonoBehaviour {

    private struct Metaball {
        public Vector3 position;
        public Vector3 velocity;
    }

    public bool redrawEveryFrame = true;
    public float isoLevel = 0.5f;
    public int resolution = 30;
    public int metaballCount = 5;

    float voxelSize;
    float[] samples;

    Surface surface;
    Mesh mesh;

    float prevIsoLevel;
    float prevResolution;

    Metaball[] metaballs;

    public float padding = 0.2f;

	void Start () {
        mesh = GetComponent<MeshFilter>().mesh;

        metaballs = new Metaball[metaballCount];
        for (int i = 0; i < metaballs.Length; i++)
        {
            metaballs[i].position = (Vector3.one + Random.insideUnitSphere * (1f - padding)) * 0.5f;
            metaballs[i].velocity = Random.insideUnitSphere * 0.1f;
        }
	}

    void Update () {
        float deltaTime = Time.deltaTime;

        for (int i = 0; i < metaballs.Length; i++)
        {
            Vector3 nextPosition = metaballs[i].position + metaballs[i].velocity * deltaTime;
            if (nextPosition.x < 0 + padding || nextPosition.x > 1 - padding) metaballs[i].velocity.x = -metaballs[i].velocity.x;
            if (nextPosition.y < 0 + padding || nextPosition.y > 1 - padding) metaballs[i].velocity.y = -metaballs[i].velocity.y;
            if (nextPosition.z < 0 + padding || nextPosition.z > 1 - padding) metaballs[i].velocity.z = -metaballs[i].velocity.z;

            metaballs[i].position += metaballs[i].velocity * deltaTime;
        }

        samples = GenerateSamples();

        if (redrawEveryFrame || isoLevel != prevIsoLevel || resolution != prevResolution)
        {
            if (resolution != prevResolution) voxelSize = 1f / resolution;

            surface = MarchingCubes.CreateSurface(samples, resolution, voxelSize, isoLevel);

            mesh.Clear();
            mesh.vertices = surface.vertices;
            mesh.normals = surface.normals;
            mesh.triangles = surface.triangles;
        }

        prevIsoLevel = isoLevel;
        prevResolution = resolution;
    }

    float[] GenerateSamples () {
        float[] samples = new float[resolution * resolution * resolution];

        for (int z = 0; z < resolution; z++)
        {
            for (int y = 0; y < resolution; y++)
            {
                for (int x = 0; x < resolution; x++)
                {
                    int index = z * resolution * resolution + y * resolution + x;
                    samples[index] = Sample(new Vector3(x, y, z) * voxelSize);
                }
            }
        }

        return samples;
    }
	
	float Sample (Vector3 position) {
        float result = 0f;
        foreach (Metaball metaball in metaballs)
        {
            float dx = (position.x - metaball.position.x);
            float dy = (position.y - metaball.position.y);
            float dz = (position.z - metaball.position.z);
            float f = 1f / ( dx * dx + dy * dy + dz * dz);

            result += f;
        }
        return result;
	}
}
