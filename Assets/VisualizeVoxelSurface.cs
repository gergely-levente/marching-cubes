﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class VisualizeVoxelSurface : MonoBehaviour {
    [Range(0, 255)]
    public int pattern = 1;

    float[] samples;

    Surface surface;
    Mesh mesh;

	void Start () {
        mesh = GetComponent<MeshFilter>().mesh;
	}

    void Update () {
        samples = new float[2 * 2 * 2] {
            (pattern >> 0) % 2,
            (pattern >> 1) % 2,
            (pattern >> 3) % 2,
            (pattern >> 2) % 2,
            (pattern >> 4) % 2,
            (pattern >> 5) % 2,
            (pattern >> 7) % 2,
            (pattern >> 6) % 2,
        };
        surface = MarchingCubes.CreateSurface(samples, 2, 1, 0.5f);

        mesh.Clear();
        mesh.vertices = surface.vertices;
        mesh.normals = surface.normals;
        mesh.triangles = surface.triangles;
    }

	float Sample (Vector3 position) {
        return Vector3.Magnitude(Vector3.one / 2f) - Vector3.Distance(Vector3.one / 2f, position);
	}

    void OnDrawGizmos () {
	    for (int z = 0; z < 2; z++)
        {
            for (int y = 0; y < 2; y++)
            {
                for (int x = 0; x < 2; x++)
                {
                    float value = samples[z * 2 * 2 + y * 2 +  x];
                    Gizmos.color = value >= 0.5f ? Color.black : Color.white;
                    Gizmos.DrawSphere(new Vector3(x, y, z), 0.05f);
                }
            }
        }

        if (mesh != null) {
            foreach (Vector3 vertex in mesh.vertices)
            {
                Gizmos.color = Color.cyan;
                Gizmos.DrawSphere(vertex, 0.05f);
            }
            
            for (int i = 0; i < mesh.triangles.Length - 2; i += 3)
            {
                int vertexIndex0 = mesh.triangles[i];
                int vertexIndex1 = mesh.triangles[i + 1];
                int vertexIndex2 = mesh.triangles[i + 2];

                Vector3 vertex0 = mesh.vertices[vertexIndex0];
                Vector3 vertex1 = mesh.vertices[vertexIndex1];
                Vector3 vertex2 = mesh.vertices[vertexIndex2];

                Debug.DrawLine(vertex0, vertex1, Color.cyan);
                Debug.DrawLine(vertex1, vertex2, Color.cyan);
                Debug.DrawLine(vertex2, vertex0, Color.cyan);
            }
        }
    }
}
