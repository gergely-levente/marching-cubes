﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class EdgeTableGenerator {

    [MenuItem("Marching Cubes/Generate Edge Table")]
	static void NewMenuOption () {
        int[] edgeTable = GenerateEdgeTable();

        string path = "Assets/Marching Cubes/EdgeTable.txt";
        StreamWriter file = new StreamWriter(path);
        foreach (int edge in edgeTable)
        {
            file.WriteLine(String.Format("0x{0:X3},", edge));
        }
        file.Close();

        Debug.Log(String.Format("Edge table written to {0}", path));
	}

    private static int[] GenerateEdgeTable() {
        int[] edgeTable = new int[256];
        for (int intersectionType = 0; intersectionType < 256; intersectionType++)
        {
            int[] edgeIndices = MarchingCubes.IntersectedEdges(intersectionType);
            int edges = 0;
            foreach (int edgeIndex in edgeIndices)
            {
                edges |= 1 << edgeIndex;
            }
            edgeTable[intersectionType] = edges;
        }

        return edgeTable;
    }

}
