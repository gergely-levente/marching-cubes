﻿using UnityEngine;
using UnityEngine.Assertions;
using System.Collections.Generic;

public struct Surface {
    public Vector3[] vertices;
    public Vector3[] normals;
    public int[] triangles;
}

public static class MarchingCubes {

    public static Surface CreateSurface (float[] samples, int resolution, float voxelSize, float isoValue) {
        List<Vector3> vertices = new List<Vector3>();
        List<Vector3> normals = new List<Vector3>();
        List<int> triangles = new List<int>();

        float[] vertexValues = new float[8];
        Vector3[] vertexNormals = new Vector3[8];
        int currentVertexCount = 0;

        int voxelCount = resolution - 1;
        for (int z = 0; z < voxelCount; z++)
        {
            for (int y = 0; y < voxelCount; y++)
            {
                for (int x = 0; x < voxelCount; x++)
                {
                    vertexValues[0] = samples[(z) * resolution * resolution + (y) * resolution + x];
                    vertexValues[1] = samples[(z) * resolution * resolution + (y) * resolution + x + 1];
                    vertexValues[2] = samples[(z) * resolution * resolution + (y + 1) * resolution + x + 1];
                    vertexValues[3] = samples[(z) * resolution * resolution + (y + 1) * resolution + x];
                    vertexValues[4] = samples[(z + 1) * resolution * resolution + (y) * resolution + x];
                    vertexValues[5] = samples[(z + 1) * resolution * resolution + (y) * resolution + x + 1];
                    vertexValues[6] = samples[(z + 1) * resolution * resolution + (y + 1) * resolution + x + 1];
                    vertexValues[7] = samples[(z + 1) * resolution * resolution + (y + 1) * resolution + x];

                    int intersectionType = 0;
                    if (vertexValues[0] >= isoValue) intersectionType |= 1 << 0;
                    if (vertexValues[1] >= isoValue) intersectionType |= 1 << 1;
                    if (vertexValues[2] >= isoValue) intersectionType |= 1 << 2;
                    if (vertexValues[3] >= isoValue) intersectionType |= 1 << 3;
                    if (vertexValues[4] >= isoValue) intersectionType |= 1 << 4;
                    if (vertexValues[5] >= isoValue) intersectionType |= 1 << 5;
                    if (vertexValues[6] >= isoValue) intersectionType |= 1 << 6;
                    if (vertexValues[7] >= isoValue) intersectionType |= 1 << 7;

                    if (intersectionType != 0 && intersectionType != 255) 
                    {
                        vertexNormals[0] = Normal(samples, resolution, x, y, z);
                        vertexNormals[1] = Normal(samples, resolution, x + 1, y, z);
                        vertexNormals[2] = Normal(samples, resolution, x + 1, y + 1, z);
                        vertexNormals[3] = Normal(samples, resolution, x, y + 1, z);
                        vertexNormals[4] = Normal(samples, resolution, x, y, z + 1);
                        vertexNormals[5] = Normal(samples, resolution, x + 1, y, z + 1);
                        vertexNormals[6] = Normal(samples, resolution, x + 1, y + 1, z + 1);
                        vertexNormals[7] = Normal(samples, resolution, x, y + 1, z + 1);

                        int[] edgeVertexIndices = new int[12];

                        for (int edge = 0; edge < 12; edge++)
                        {
                            bool isEdgeIntersected = ((1 << edge) & edgeTable[intersectionType]) == (1 << edge);
                            if (isEdgeIntersected) {
                                int[] edgeVertices = edgeConnections[edge];

                                int vertexIndexA = edgeVertices[0];
                                int vertexIndexB = edgeVertices[1];

                                float valueA = vertexValues[vertexIndexA];
                                float valueB = vertexValues[vertexIndexB];

                                float t = (isoValue - valueA) / (valueB - valueA);

                                Vector3 vertexA =  new Vector3(x, y, z) + vertexOffsets[vertexIndexA];
                                Vector3 edgeDirection = edgeDirections[edge];
                                Vector3 edgeVertexPosition = vertexA + edgeDirection * t;

                                Vector3 normalA = vertexNormals[vertexIndexA];
                                Vector3 normalB = vertexNormals[vertexIndexB];
                                // NOTE: LerpUnclamped() is much faster than Lerp()
                                // clamping is not required because 0 <= t <= 1 is always true for intersected edges
                                Vector3 edgeVertexNormal = Vector3.LerpUnclamped(normalA, normalB, t);

                                vertices.Add(edgeVertexPosition * voxelSize);
                                normals.Add(edgeVertexNormal);

                                edgeVertexIndices[edge] = currentVertexCount++;
                            }
                        }

                        foreach (int edgeIndex in triangulations[intersectionType])
                        {
                            triangles.Add(edgeVertexIndices[edgeIndex]);
                        }
                    }
                }
            }
        }

        Surface surface;
        surface.vertices = vertices.ToArray();
        surface.normals = normals.ToArray();
        surface.triangles = triangles.ToArray();

        return surface;
    }

    private static Vector3 Normal (float[] samples, int resolution, int x, int y, int z) {
        int prevX = x == 0 ? x : x - 1;
        int nextX = x == resolution - 1 ? x : x + 1;
        int prevY = y == 0 ? y : y - 1;
        int nextY = y == resolution - 1 ? y : y + 1;
        int prevZ = z == 0 ? z : z - 1;
        int nextZ = z == resolution - 1 ? z : z + 1;

        float prevValueX = samples[z * resolution * resolution + y * resolution + prevX];
        float nextValueX = samples[z * resolution * resolution + y * resolution + nextX];
        float prevValueY = samples[z * resolution * resolution + prevY * resolution + x];
        float nextValueY = samples[z * resolution * resolution + nextY * resolution + x];
        float prevValueZ = samples[prevZ * resolution * resolution + y * resolution + x];
        float nextValueZ = samples[nextZ * resolution * resolution + y * resolution + x];

        float gradX = (prevValueX - nextValueX) / (nextX - prevX);
        float gradY = (prevValueY - nextValueY) / (nextY - prevY);
        float gradZ = (prevValueZ - nextValueZ) / (nextZ - prevZ);

        return new Vector3(gradX, gradY, gradZ);
    }

    public static int[] IntersectedEdges (int intersectionType) {
        List<int> intersectedEdges = new List<int>(12);

        for (int i = 0; i < edgeConnections.Length; i++)
        {
            int indexA = edgeConnections[i][0];
            int indexB = edgeConnections[i][1];

            bool isPointAInside = (1 << indexA) == (intersectionType & (1 << indexA));
            bool isPointBInside = (1 << indexB) == (intersectionType & (1 << indexB));

            if (isPointAInside != isPointBInside)
            {
                intersectedEdges.Add(i);
            }
        }

        return intersectedEdges.ToArray();
    }

    public static readonly int[][] edgeConnections = new int[][] {
        new int[] {0, 1}, new int[] {1, 2}, new int[] {2, 3}, new int[] {3, 0},
        new int[] {4, 5}, new int[] {5, 6}, new int[] {6, 7}, new int[] {7, 4},
        new int[] {0, 4}, new int[] {1, 5}, new int[] {3, 7}, new int[] {2, 6}
    };

    private static readonly Vector3[] edgeDirections = new Vector3[] {
        Vector3.right,      Vector3.up,         Vector3.left,       Vector3.down,
        Vector3.right,      Vector3.up,         Vector3.left,       Vector3.down,
        Vector3.forward,    Vector3.forward,    Vector3.forward,    Vector3.forward,
    };

    private static readonly Vector3[] vertexOffsets = new Vector3[] {
        new Vector3(0, 0, 0),
        new Vector3(1, 0, 0),
        new Vector3(1, 1, 0),
        new Vector3(0, 1, 0),
        new Vector3(0, 0, 1),
        new Vector3(1, 0, 1),
        new Vector3(1, 1, 1),
        new Vector3(0, 1, 1)
    };

    private static readonly int[] edgeTable  = new int[256] {
        0x000, 0x109, 0x203, 0x30A, 0x806, 0x90F, 0xA05, 0xB0C, 0x40C, 0x505, 0x60F, 0x706, 0xC0A, 0xD03, 0xE09, 0xF00,
        0x190, 0x099, 0x393, 0x29A, 0x996, 0x89F, 0xB95, 0xA9C, 0x59C, 0x495, 0x79F, 0x696, 0xD9A, 0xC93, 0xF99, 0xE90,
        0x230, 0x339, 0x033, 0x13A, 0xA36, 0xB3F, 0x835, 0x93C, 0x63C, 0x735, 0x43F, 0x536, 0xE3A, 0xF33, 0xC39, 0xD30,
        0x3A0, 0x2A9, 0x1A3, 0x0AA, 0xBA6, 0xAAF, 0x9A5, 0x8AC, 0x7AC, 0x6A5, 0x5AF, 0x4A6, 0xFAA, 0xEA3, 0xDA9, 0xCA0,
        0x860, 0x969, 0xA63, 0xB6A, 0x066, 0x16F, 0x265, 0x36C, 0xC6C, 0xD65, 0xE6F, 0xF66, 0x46A, 0x563, 0x669, 0x760,
        0x9F0, 0x8F9, 0xBF3, 0xAFA, 0x1F6, 0x0FF, 0x3F5, 0x2FC, 0xDFC, 0xCF5, 0xFFF, 0xEF6, 0x5FA, 0x4F3, 0x7F9, 0x6F0,
        0xA50, 0xB59, 0x853, 0x95A, 0x256, 0x35F, 0x055, 0x15C, 0xE5C, 0xF55, 0xC5F, 0xD56, 0x65A, 0x753, 0x459, 0x550,
        0xBC0, 0xAC9, 0x9C3, 0x8CA, 0x3C6, 0x2CF, 0x1C5, 0x0CC, 0xFCC, 0xEC5, 0xDCF, 0xCC6, 0x7CA, 0x6C3, 0x5C9, 0x4C0,
        0x4C0, 0x5C9, 0x6C3, 0x7CA, 0xCC6, 0xDCF, 0xEC5, 0xFCC, 0x0CC, 0x1C5, 0x2CF, 0x3C6, 0x8CA, 0x9C3, 0xAC9, 0xBC0,
        0x550, 0x459, 0x753, 0x65A, 0xD56, 0xC5F, 0xF55, 0xE5C, 0x15C, 0x055, 0x35F, 0x256, 0x95A, 0x853, 0xB59, 0xA50,
        0x6F0, 0x7F9, 0x4F3, 0x5FA, 0xEF6, 0xFFF, 0xCF5, 0xDFC, 0x2FC, 0x3F5, 0x0FF, 0x1F6, 0xAFA, 0xBF3, 0x8F9, 0x9F0,
        0x760, 0x669, 0x563, 0x46A, 0xF66, 0xE6F, 0xD65, 0xC6C, 0x36C, 0x265, 0x16F, 0x066, 0xB6A, 0xA63, 0x969, 0x860,
        0xCA0, 0xDA9, 0xEA3, 0xFAA, 0x4A6, 0x5AF, 0x6A5, 0x7AC, 0x8AC, 0x9A5, 0xAAF, 0xBA6, 0x0AA, 0x1A3, 0x2A9, 0x3A0,
        0xD30, 0xC39, 0xF33, 0xE3A, 0x536, 0x43F, 0x735, 0x63C, 0x93C, 0x835, 0xB3F, 0xA36, 0x13A, 0x033, 0x339, 0x230,
        0xE90, 0xF99, 0xC93, 0xD9A, 0x696, 0x79F, 0x495, 0x59C, 0xA9C, 0xB95, 0x89F, 0x996, 0x29A, 0x393, 0x099, 0x190,
        0xF00, 0xE09, 0xD03, 0xC0A, 0x706, 0x60F, 0x505, 0x40C, 0xB0C, 0xA05, 0x90F, 0x806, 0x30A, 0x203, 0x109, 0x000,
    };

    private static readonly int[][] triangulations  = new int[][] {
        new int[] {},
        new int[] { 0, 3, 8, },
        new int[] { 0, 9, 1, },
        new int[] { 1, 3, 8, 9, 1, 8, },
        new int[] { 1, 11, 2, },
        new int[] { 0, 3, 8, 1, 11, 2, },
        new int[] { 9, 11, 2, 0, 9, 2, },
        new int[] { 2, 3, 8, 2, 8, 11, 11, 8, 9, },
        new int[] { 3, 2, 10, },
        new int[] { 0, 2, 10, 8, 0, 10, },
        new int[] { 1, 0, 9, 2, 10, 3, },
        new int[] { 1, 2, 10, 1, 10, 9, 9, 10, 8, },
        new int[] { 3, 1, 11, 10, 3, 11, },
        new int[] { 0, 1, 11, 0, 11, 8, 8, 11, 10, },
        new int[] { 3, 0, 9, 3, 9, 10, 10, 9, 11, },
        new int[] { 9, 11, 8, 11, 10, 8, },
        new int[] { 4, 8, 7, },
        new int[] { 4, 0, 3, 7, 4, 3, },
        new int[] { 0, 9, 1, 8, 7, 4, },
        new int[] { 4, 9, 1, 4, 1, 7, 7, 1, 3, },
        new int[] { 1, 11, 2, 8, 7, 4, },
        new int[] { 3, 7, 4, 3, 4, 0, 1, 11, 2, },
        new int[] { 9, 11, 2, 9, 2, 0, 8, 7, 4, },
        new int[] { 2, 9, 11, 2, 7, 9, 2, 3, 7, 7, 4, 9, },
        new int[] { 8, 7, 4, 3, 2, 10, },
        new int[] { 10, 7, 4, 10, 4, 2, 2, 4, 0, },
        new int[] { 9, 1, 0, 8, 7, 4, 2, 10, 3, },
        new int[] { 4, 10, 7, 9, 10, 4, 9, 2, 10, 9, 1, 2, },
        new int[] { 3, 1, 11, 3, 11, 10, 7, 4, 8, },
        new int[] { 1, 11, 10, 1, 10, 4, 1, 4, 0, 7, 4, 10, },
        new int[] { 4, 8, 7, 9, 10, 0, 9, 11, 10, 10, 3, 0, },
        new int[] { 4, 10, 7, 4, 9, 10, 9, 11, 10, },
        new int[] { 9, 4, 5, },
        new int[] { 9, 4, 5, 0, 3, 8, },
        new int[] { 0, 4, 5, 1, 0, 5, },
        new int[] { 8, 4, 5, 8, 5, 3, 3, 5, 1, },
        new int[] { 1, 11, 2, 9, 4, 5, },
        new int[] { 3, 8, 0, 1, 11, 2, 4, 5, 9, },
        new int[] { 5, 11, 2, 5, 2, 4, 4, 2, 0, },
        new int[] { 2, 5, 11, 3, 5, 2, 3, 4, 5, 3, 8, 4, },
        new int[] { 9, 4, 5, 2, 10, 3, },
        new int[] { 0, 2, 10, 0, 10, 8, 4, 5, 9, },
        new int[] { 0, 4, 5, 0, 5, 1, 2, 10, 3, },
        new int[] { 2, 5, 1, 2, 8, 5, 2, 10, 8, 4, 5, 8, },
        new int[] { 11, 10, 3, 11, 3, 1, 9, 4, 5, },
        new int[] { 4, 5, 9, 0, 1, 8, 8, 1, 11, 8, 11, 10, },
        new int[] { 5, 0, 4, 5, 10, 0, 5, 11, 10, 10, 3, 0, },
        new int[] { 5, 8, 4, 5, 11, 8, 11, 10, 8, },
        new int[] { 9, 8, 7, 5, 9, 7, },
        new int[] { 9, 0, 3, 9, 3, 5, 5, 3, 7, },
        new int[] { 0, 8, 7, 0, 7, 1, 1, 7, 5, },
        new int[] { 1, 3, 5, 3, 7, 5, },
        new int[] { 9, 8, 7, 9, 7, 5, 11, 2, 1, },
        new int[] { 11, 2, 1, 9, 0, 5, 5, 0, 3, 5, 3, 7, },
        new int[] { 8, 2, 0, 8, 5, 2, 8, 7, 5, 11, 2, 5, },
        new int[] { 2, 5, 11, 2, 3, 5, 3, 7, 5, },
        new int[] { 7, 5, 9, 7, 9, 8, 3, 2, 10, },
        new int[] { 9, 7, 5, 9, 2, 7, 9, 0, 2, 2, 10, 7, },
        new int[] { 2, 10, 3, 0, 8, 1, 1, 8, 7, 1, 7, 5, },
        new int[] { 10, 1, 2, 10, 7, 1, 7, 5, 1, },
        new int[] { 9, 8, 5, 8, 7, 5, 11, 3, 1, 11, 10, 3, },
        new int[] { 5, 0, 7, 5, 9, 0, 7, 0, 10, 1, 11, 0, 10, 0, 11, },
        new int[] { 10, 0, 11, 10, 3, 0, 11, 0, 5, 8, 7, 0, 5, 0, 7, },
        new int[] { 10, 5, 11, 7, 5, 10, },
        new int[] { 11, 5, 6, },
        new int[] { 0, 3, 8, 5, 6, 11, },
        new int[] { 9, 1, 0, 5, 6, 11, },
        new int[] { 1, 3, 8, 1, 8, 9, 5, 6, 11, },
        new int[] { 1, 5, 6, 2, 1, 6, },
        new int[] { 1, 5, 6, 1, 6, 2, 3, 8, 0, },
        new int[] { 9, 5, 6, 9, 6, 0, 0, 6, 2, },
        new int[] { 5, 8, 9, 5, 2, 8, 5, 6, 2, 3, 8, 2, },
        new int[] { 2, 10, 3, 11, 5, 6, },
        new int[] { 10, 8, 0, 10, 0, 2, 11, 5, 6, },
        new int[] { 0, 9, 1, 2, 10, 3, 5, 6, 11, },
        new int[] { 5, 6, 11, 1, 2, 9, 9, 2, 10, 9, 10, 8, },
        new int[] { 6, 10, 3, 6, 3, 5, 5, 3, 1, },
        new int[] { 0, 10, 8, 0, 5, 10, 0, 1, 5, 5, 6, 10, },
        new int[] { 3, 6, 10, 0, 6, 3, 0, 5, 6, 0, 9, 5, },
        new int[] { 6, 9, 5, 6, 10, 9, 10, 8, 9, },
        new int[] { 5, 6, 11, 4, 8, 7, },
        new int[] { 4, 0, 3, 4, 3, 7, 6, 11, 5, },
        new int[] { 1, 0, 9, 5, 6, 11, 8, 7, 4, },
        new int[] { 11, 5, 6, 1, 7, 9, 1, 3, 7, 7, 4, 9, },
        new int[] { 6, 2, 1, 6, 1, 5, 4, 8, 7, },
        new int[] { 1, 5, 2, 5, 6, 2, 3, 4, 0, 3, 7, 4, },
        new int[] { 8, 7, 4, 9, 5, 0, 0, 5, 6, 0, 6, 2, },
        new int[] { 7, 9, 3, 7, 4, 9, 3, 9, 2, 5, 6, 9, 2, 9, 6, },
        new int[] { 3, 2, 10, 7, 4, 8, 11, 5, 6, },
        new int[] { 5, 6, 11, 4, 2, 7, 4, 0, 2, 2, 10, 7, },
        new int[] { 0, 9, 1, 4, 8, 7, 2, 10, 3, 5, 6, 11, },
        new int[] { 9, 1, 2, 9, 2, 10, 9, 10, 4, 7, 4, 10, 5, 6, 11, },
        new int[] { 8, 7, 4, 3, 5, 10, 3, 1, 5, 5, 6, 10, },
        new int[] { 5, 10, 1, 5, 6, 10, 1, 10, 0, 7, 4, 10, 0, 10, 4, },
        new int[] { 0, 9, 5, 0, 5, 6, 0, 6, 3, 10, 3, 6, 8, 7, 4, },
        new int[] { 6, 9, 5, 6, 10, 9, 4, 9, 7, 7, 9, 10, },
        new int[] { 11, 9, 4, 6, 11, 4, },
        new int[] { 4, 6, 11, 4, 11, 9, 0, 3, 8, },
        new int[] { 11, 1, 0, 11, 0, 6, 6, 0, 4, },
        new int[] { 8, 1, 3, 8, 6, 1, 8, 4, 6, 6, 11, 1, },
        new int[] { 1, 9, 4, 1, 4, 2, 2, 4, 6, },
        new int[] { 3, 8, 0, 1, 9, 2, 2, 9, 4, 2, 4, 6, },
        new int[] { 0, 4, 2, 4, 6, 2, },
        new int[] { 8, 2, 3, 8, 4, 2, 4, 6, 2, },
        new int[] { 11, 9, 4, 11, 4, 6, 10, 3, 2, },
        new int[] { 0, 2, 8, 2, 10, 8, 4, 11, 9, 4, 6, 11, },
        new int[] { 3, 2, 10, 0, 6, 1, 0, 4, 6, 6, 11, 1, },
        new int[] { 6, 1, 4, 6, 11, 1, 4, 1, 8, 2, 10, 1, 8, 1, 10, },
        new int[] { 9, 4, 6, 9, 6, 3, 9, 3, 1, 10, 3, 6, },
        new int[] { 8, 1, 10, 8, 0, 1, 10, 1, 6, 9, 4, 1, 6, 1, 4, },
        new int[] { 3, 6, 10, 3, 0, 6, 0, 4, 6, },
        new int[] { 6, 8, 4, 10, 8, 6, },
        new int[] { 7, 6, 11, 7, 11, 8, 8, 11, 9, },
        new int[] { 0, 3, 7, 0, 7, 11, 0, 11, 9, 6, 11, 7, },
        new int[] { 11, 7, 6, 1, 7, 11, 1, 8, 7, 1, 0, 8, },
        new int[] { 11, 7, 6, 11, 1, 7, 1, 3, 7, },
        new int[] { 1, 6, 2, 1, 8, 6, 1, 9, 8, 8, 7, 6, },
        new int[] { 2, 9, 6, 2, 1, 9, 6, 9, 7, 0, 3, 9, 7, 9, 3, },
        new int[] { 7, 0, 8, 7, 6, 0, 6, 2, 0, },
        new int[] { 7, 2, 3, 6, 2, 7, },
        new int[] { 2, 10, 3, 11, 8, 6, 11, 9, 8, 8, 7, 6, },
        new int[] { 2, 7, 0, 2, 10, 7, 0, 7, 9, 6, 11, 7, 9, 7, 11, },
        new int[] { 1, 0, 8, 1, 8, 7, 1, 7, 11, 6, 11, 7, 2, 10, 3, },
        new int[] { 10, 1, 2, 10, 7, 1, 11, 1, 6, 6, 1, 7, },
        new int[] { 8, 6, 9, 8, 7, 6, 9, 6, 1, 10, 3, 6, 1, 6, 3, },
        new int[] { 0, 1, 9, 10, 7, 6, },
        new int[] { 7, 0, 8, 7, 6, 0, 3, 0, 10, 10, 0, 6, },
        new int[] { 7, 6, 10, },
        new int[] { 7, 10, 6, },
        new int[] { 3, 8, 0, 10, 6, 7, },
        new int[] { 0, 9, 1, 10, 6, 7, },
        new int[] { 8, 9, 1, 8, 1, 3, 10, 6, 7, },
        new int[] { 11, 2, 1, 6, 7, 10, },
        new int[] { 1, 11, 2, 3, 8, 0, 6, 7, 10, },
        new int[] { 2, 0, 9, 2, 9, 11, 6, 7, 10, },
        new int[] { 6, 7, 10, 2, 3, 11, 11, 3, 8, 11, 8, 9, },
        new int[] { 7, 3, 2, 6, 7, 2, },
        new int[] { 7, 8, 0, 7, 0, 6, 6, 0, 2, },
        new int[] { 2, 6, 7, 2, 7, 3, 0, 9, 1, },
        new int[] { 1, 2, 6, 1, 6, 8, 1, 8, 9, 8, 6, 7, },
        new int[] { 11, 6, 7, 11, 7, 1, 1, 7, 3, },
        new int[] { 11, 6, 7, 1, 11, 7, 1, 7, 8, 1, 8, 0, },
        new int[] { 0, 7, 3, 0, 11, 7, 0, 9, 11, 6, 7, 11, },
        new int[] { 7, 11, 6, 7, 8, 11, 8, 9, 11, },
        new int[] { 6, 4, 8, 10, 6, 8, },
        new int[] { 3, 10, 6, 3, 6, 0, 0, 6, 4, },
        new int[] { 8, 10, 6, 8, 6, 4, 9, 1, 0, },
        new int[] { 9, 6, 4, 9, 3, 6, 9, 1, 3, 10, 6, 3, },
        new int[] { 6, 4, 8, 6, 8, 10, 2, 1, 11, },
        new int[] { 1, 11, 2, 3, 10, 0, 0, 10, 6, 0, 6, 4, },
        new int[] { 4, 8, 10, 4, 10, 6, 0, 9, 2, 2, 9, 11, },
        new int[] { 11, 3, 9, 11, 2, 3, 9, 3, 4, 10, 6, 3, 4, 3, 6, },
        new int[] { 8, 3, 2, 8, 2, 4, 4, 2, 6, },
        new int[] { 0, 2, 4, 4, 2, 6, },
        new int[] { 1, 0, 9, 2, 4, 3, 2, 6, 4, 4, 8, 3, },
        new int[] { 1, 4, 9, 1, 2, 4, 2, 6, 4, },
        new int[] { 8, 3, 1, 8, 1, 6, 8, 6, 4, 6, 1, 11, },
        new int[] { 11, 0, 1, 11, 6, 0, 6, 4, 0, },
        new int[] { 4, 3, 6, 4, 8, 3, 6, 3, 11, 0, 9, 3, 11, 3, 9, },
        new int[] { 11, 4, 9, 6, 4, 11, },
        new int[] { 4, 5, 9, 7, 10, 6, },
        new int[] { 0, 3, 8, 4, 5, 9, 10, 6, 7, },
        new int[] { 5, 1, 0, 5, 0, 4, 7, 10, 6, },
        new int[] { 10, 6, 7, 8, 4, 3, 3, 4, 5, 3, 5, 1, },
        new int[] { 9, 4, 5, 11, 2, 1, 7, 10, 6, },
        new int[] { 6, 7, 10, 1, 11, 2, 0, 3, 8, 4, 5, 9, },
        new int[] { 7, 10, 6, 5, 11, 4, 4, 11, 2, 4, 2, 0, },
        new int[] { 3, 8, 4, 3, 4, 5, 3, 5, 2, 11, 2, 5, 10, 6, 7, },
        new int[] { 7, 3, 2, 7, 2, 6, 5, 9, 4, },
        new int[] { 9, 4, 5, 0, 6, 8, 0, 2, 6, 6, 7, 8, },
        new int[] { 3, 2, 6, 3, 6, 7, 1, 0, 5, 5, 0, 4, },
        new int[] { 6, 8, 2, 6, 7, 8, 2, 8, 1, 4, 5, 8, 1, 8, 5, },
        new int[] { 9, 4, 5, 11, 6, 1, 1, 6, 7, 1, 7, 3, },
        new int[] { 1, 11, 6, 1, 6, 7, 1, 7, 0, 8, 0, 7, 9, 4, 5, },
        new int[] { 4, 11, 0, 4, 5, 11, 0, 11, 3, 6, 7, 11, 3, 11, 7, },
        new int[] { 7, 11, 6, 7, 8, 11, 5, 11, 4, 4, 11, 8, },
        new int[] { 6, 5, 9, 6, 9, 10, 10, 9, 8, },
        new int[] { 3, 10, 6, 0, 3, 6, 0, 6, 5, 0, 5, 9, },
        new int[] { 0, 8, 10, 0, 10, 5, 0, 5, 1, 5, 10, 6, },
        new int[] { 6, 3, 10, 6, 5, 3, 5, 1, 3, },
        new int[] { 1, 11, 2, 9, 10, 5, 9, 8, 10, 10, 6, 5, },
        new int[] { 0, 3, 10, 0, 10, 6, 0, 6, 9, 5, 9, 6, 1, 11, 2, },
        new int[] { 10, 5, 8, 10, 6, 5, 8, 5, 0, 11, 2, 5, 0, 5, 2, },
        new int[] { 6, 3, 10, 6, 5, 3, 2, 3, 11, 11, 3, 5, },
        new int[] { 5, 9, 8, 5, 8, 2, 5, 2, 6, 3, 2, 8, },
        new int[] { 9, 6, 5, 9, 0, 6, 0, 2, 6, },
        new int[] { 1, 8, 5, 1, 0, 8, 5, 8, 6, 3, 2, 8, 6, 8, 2, },
        new int[] { 1, 6, 5, 2, 6, 1, },
        new int[] { 1, 6, 3, 1, 11, 6, 3, 6, 8, 5, 9, 6, 8, 6, 9, },
        new int[] { 11, 0, 1, 11, 6, 0, 9, 0, 5, 5, 0, 6, },
        new int[] { 0, 8, 3, 5, 11, 6, },
        new int[] { 11, 6, 5, },
        new int[] { 10, 11, 5, 7, 10, 5, },
        new int[] { 10, 11, 5, 10, 5, 7, 8, 0, 3, },
        new int[] { 5, 7, 10, 5, 10, 11, 1, 0, 9, },
        new int[] { 11, 5, 7, 11, 7, 10, 9, 1, 8, 8, 1, 3, },
        new int[] { 10, 2, 1, 10, 1, 7, 7, 1, 5, },
        new int[] { 0, 3, 8, 1, 7, 2, 1, 5, 7, 7, 10, 2, },
        new int[] { 9, 5, 7, 9, 7, 2, 9, 2, 0, 2, 7, 10, },
        new int[] { 7, 2, 5, 7, 10, 2, 5, 2, 9, 3, 8, 2, 9, 2, 8, },
        new int[] { 2, 11, 5, 2, 5, 3, 3, 5, 7, },
        new int[] { 8, 0, 2, 8, 2, 5, 8, 5, 7, 11, 5, 2, },
        new int[] { 9, 1, 0, 5, 3, 11, 5, 7, 3, 3, 2, 11, },
        new int[] { 9, 2, 8, 9, 1, 2, 8, 2, 7, 11, 5, 2, 7, 2, 5, },
        new int[] { 1, 5, 3, 3, 5, 7, },
        new int[] { 0, 7, 8, 0, 1, 7, 1, 5, 7, },
        new int[] { 9, 3, 0, 9, 5, 3, 5, 7, 3, },
        new int[] { 9, 7, 8, 5, 7, 9, },
        new int[] { 5, 4, 8, 5, 8, 11, 11, 8, 10, },
        new int[] { 5, 4, 0, 5, 0, 10, 5, 10, 11, 10, 0, 3, },
        new int[] { 0, 9, 1, 8, 11, 4, 8, 10, 11, 11, 5, 4, },
        new int[] { 11, 4, 10, 11, 5, 4, 10, 4, 3, 9, 1, 4, 3, 4, 1, },
        new int[] { 2, 1, 5, 2, 5, 8, 2, 8, 10, 4, 8, 5, },
        new int[] { 0, 10, 4, 0, 3, 10, 4, 10, 5, 2, 1, 10, 5, 10, 1, },
        new int[] { 0, 5, 2, 0, 9, 5, 2, 5, 10, 4, 8, 5, 10, 5, 8, },
        new int[] { 9, 5, 4, 2, 3, 10, },
        new int[] { 2, 11, 5, 3, 2, 5, 3, 5, 4, 3, 4, 8, },
        new int[] { 5, 2, 11, 5, 4, 2, 4, 0, 2, },
        new int[] { 3, 2, 11, 3, 11, 5, 3, 5, 8, 4, 8, 5, 0, 9, 1, },
        new int[] { 5, 2, 11, 5, 4, 2, 1, 2, 9, 9, 2, 4, },
        new int[] { 8, 5, 4, 8, 3, 5, 3, 1, 5, },
        new int[] { 0, 5, 4, 1, 5, 0, },
        new int[] { 8, 5, 4, 8, 3, 5, 9, 5, 0, 0, 5, 3, },
        new int[] { 9, 5, 4, },
        new int[] { 4, 7, 10, 4, 10, 9, 9, 10, 11, },
        new int[] { 0, 3, 8, 4, 7, 9, 9, 7, 10, 9, 10, 11, },
        new int[] { 1, 10, 11, 1, 4, 10, 1, 0, 4, 7, 10, 4, },
        new int[] { 3, 4, 1, 3, 8, 4, 1, 4, 11, 7, 10, 4, 11, 4, 10, },
        new int[] { 4, 7, 10, 9, 4, 10, 9, 10, 2, 9, 2, 1, },
        new int[] { 9, 4, 7, 9, 7, 10, 9, 10, 1, 2, 1, 10, 0, 3, 8, },
        new int[] { 10, 4, 7, 10, 2, 4, 2, 0, 4, },
        new int[] { 10, 4, 7, 10, 2, 4, 8, 4, 3, 3, 4, 2, },
        new int[] { 2, 11, 9, 2, 9, 7, 2, 7, 3, 7, 9, 4, },
        new int[] { 9, 7, 11, 9, 4, 7, 11, 7, 2, 8, 0, 7, 2, 7, 0, },
        new int[] { 3, 11, 7, 3, 2, 11, 7, 11, 4, 1, 0, 11, 4, 11, 0, },
        new int[] { 1, 2, 11, 8, 4, 7, },
        new int[] { 4, 1, 9, 4, 7, 1, 7, 3, 1, },
        new int[] { 4, 1, 9, 4, 7, 1, 0, 1, 8, 8, 1, 7, },
        new int[] { 4, 3, 0, 7, 3, 4, },
        new int[] { 4, 7, 8, },
        new int[] { 9, 8, 11, 11, 8, 10, },
        new int[] { 3, 9, 0, 3, 10, 9, 10, 11, 9, },
        new int[] { 0, 11, 1, 0, 8, 11, 8, 10, 11, },
        new int[] { 3, 11, 1, 10, 11, 3, },
        new int[] { 1, 10, 2, 1, 9, 10, 9, 8, 10, },
        new int[] { 3, 9, 0, 3, 10, 9, 1, 9, 2, 2, 9, 10, },
        new int[] { 0, 10, 2, 8, 10, 0, },
        new int[] { 3, 10, 2, },
        new int[] { 2, 8, 3, 2, 11, 8, 11, 9, 8, },
        new int[] { 9, 2, 11, 0, 2, 9, },
        new int[] { 2, 8, 3, 2, 8, 0, 11, 1, 1, 8, 11, 8, }, 
        new int[] { 1, 2, 11, },
        new int[] { 1, 8, 3, 9, 8, 1, },
        new int[] { 0, 1, 9, },
        new int[] { 0, 8, 3, },
        new int[] {},
    };
}
